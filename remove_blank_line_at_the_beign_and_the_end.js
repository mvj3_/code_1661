var v = this.current_textarea_dom.val().split("\n");

// remove blank line at the beign and the end
var _length_pre, _length;
while (_length_pre != v.length) {
  _length_pre = v.length;
  if (_.isEmpty(v[0])) { v.shift(); }
  if (_.isEmpty(v[v.length-1])) { v.pop(); }
}